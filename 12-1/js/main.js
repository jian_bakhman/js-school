
/*scroll to top*/

$(document).ready(function(){
	$(function () {
		/*price range*/

		AJAX('data/init.json', 'GET', function(data) {
			var rangeData = JSON.parse(data);
			AJAX('templates/tpl-menu-pricerange.html', 'GET', function(data){
				var rangeTPLCompiled = Handlebars.compile(data),
					priceRangehtml = rangeTPLCompiled(rangeData),
					priceRangeElem = document.getElementsByClassName("price-range");
				priceRangeElem[0].innerHTML = priceRangehtml;

				$('#sl2').slider();
			});
		});

		var RGBChange = function() {
			$('#RGB').css('background', 'rgb('+r.getValue()+','+g.getValue()+','+b.getValue()+')')
		};

		$.scrollUp({
	        scrollName: 'scrollUp', // Element ID
	        scrollDistance: 300, // Distance from top/bottom before showing element (px)
	        scrollFrom: 'top', // 'top' or 'bottom'
	        scrollSpeed: 300, // Speed back to top (ms)
	        easingType: 'linear', // Scroll to top easing (see http://easings.net/)
	        animation: 'fade', // Fade, slide, none
	        animationSpeed: 200, // Animation in speed (ms)
	        scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
					//scrollTarget: false, // Set a custom target element for scrolling to the top
	        scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
	        scrollTitle: false, // Set a custom <a> title if required.
	        scrollImg: false, // Set true to use image
	        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	        zIndex: 2147483647 // Z-Index for the overlay
		});
	});
});

AJAX('data/init.json', 'GET', function (data){
	initData = JSON.parse(data);

	AJAX('templates/tpl-categories.html', 'GET', function(data){
		var catTPL = Handlebars.compile(data),
			html = catTPL(initData),
			categoriesElem = document.getElementsByClassName("left-sidebar");
		categoriesElem[0].innerHTML = html + categoriesElem[0].innerHTML;
	});

	AJAX('templates/tpl-menu-brands.html', 'GET', function(data){
		var brandsTPL = Handlebars.compile(data),
			brandsHTML = brandsTPL(initData),
			brandsElem = document.getElementsByClassName("brands_products");
		brandsElem[0].innerHTML = brandsHTML;
	});

	AJAX('templates/tpl-features.html', 'GET', function(data){
		var featuresTPL = Handlebars.compile(data),
			featuresHTML = featuresTPL(initData),
			featuresElem = document.getElementsByClassName("features_items");
		featuresElem[0].innerHTML = featuresElem[0].innerHTML + featuresHTML;
	});
});