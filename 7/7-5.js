/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Home work

var TArray = function(type) {
//	var arrayType = type;
	
    this.push = function(){
		var arrayLength = this.length;
        
		for (i = 0; i < arguments.length; i++) {
			if (typeof arguments[i] == type) {
				console.log(arguments[i]);
				TArray.prototype.push.call(this, arguments[i]);
			}
		}
		console.log('l = ', this.length);
		console.log('n = ', arguments.length);
		if (this.length - arrayLength != arguments.length) {
			console.warn("Some arguments are wrong type in PUSH method");
		}
	};
	
    this.unshift = function(){
		var arrayLength = this.length;
        
		for (i = 0; i < arguments.length; i++) {
			if (typeof arguments[i] == type) {
				console.log(arguments[i]);
				TArray.prototype.unshift.call(this, arguments[i]);
			}
		}
		console.log('l = ', this.length);
		console.log('n = ', arguments.length);
		if (this.length - arrayLength != arguments.length) {
			console.warn("Some arguments are wrong type in UNSHIFT method");
		}
    };
	
    this.splice = function(){
		var tempArray = [];
		
		for (i = 0; i < arguments.length; i++) {
			if (typeof arguments[i] == type) {
				console.log(arguments[i]);
				tempArray.push(arguments[i]);
			}
		}
		TArray.prototype.splice.apply(this, tempArray);
		
		if (tempArray.length != arguments.length) {
			console.warn("Some arguments are wrong type in SPLICE method");
		}
    };
	
    this.concat = function(){
        var tempArr = [];
		var tempArg = [];
		
		for (var i = 0; i < arguments.length; i++) {
			if (typeof arguments[i] == type) {
				console.log(arguments[i]);
				tempArg[i] = arguments[i];
			} else if (Array.isArray(arguments[i])) {
				tempArg[i] = [];
				for (var j = 0; j < arguments[i].length; j++) {
                    if (typeof arguments[i][j] == type) {
                        console.log(arguments[i][j]);
                        tempArg[i].push(arguments[i][j]);
                    }
				}
				console.log(tempArg[i]);
			}
		}
		
		console.log(tempArg);
//		tempArr = TArray.prototype.push.apply(this, tempArg.toString());
//		tempArr = TArray.prototype.push.apply(this, tempArg);
		for (i = 0; i < tempArg.length; i++) {
            console.log(tempArg[i]);
            tempArr = TArray.prototype.push.call(this, tempArg[i]);
        }
    		
		if (this.length != arguments.length) {
			console.warn("Some arguments are wrong type in CONCAT method");
		}
		return tempArr;
    };
};
TArray.prototype = [];

var nArray = new TArray("number");
var bArray = new TArray("boolean");

//nArray.push(6, 5, 'f', 4, 3, 'a', 2, 1);
nArray.push(6, 5, 4, 3, 2, 1);
//nArray.unshift('f', 41, 3, 'a', 21);
//nArray.unshift(41, 31, 21);
//nArray.splice(2, 3, 312, 'sfd', 526);

console.log(nArray.concat([53,76,24],[534,'err', 765,'err'],7458));

var ar = [9568,5486];
var ar1 = [3,5,7,8];
var ar2 = [43,65,36];
console.log(ar.concat(ar1, ar2, 967));