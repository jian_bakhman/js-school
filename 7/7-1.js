/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// замыкание
/**
 * Comment
 */
function name() {
	var val = 1;
	return val;
}

function calc() {
	var result = 0;
	
	return {
		add: function (val) {
			result += val;
		},
		subs: function (val) {
			result -= val;
		},
		show: function (val) {
			return result;
		}
	};
}

//   ООП

//наследование

/**
 * Comment
 */
function a() {
	var result = 0;
	this.show = function () {
		return result;
	};
}

var Calc = function() {
 var result = 0;

 this.add = function(val) {
  result += val;
 }
};

var c = new Calc();
var c2 = Calc();

var b = (c instanceof Calc);



var Calc = function() {
 var result = 0;

 this.add = function(val) {
  result += val;
 }
};

var obj = {};
Calc.call(obj);
var c = Calc.call({});


/*
 *  Наследование
 */

var Calc = function() {
 this.result = 0;

 this.add = function(val) {
  result += val;
 }
};

//Наследование вар.1
var SciCalc = function () {
	Calc.call(this);
	
	this.sin = function () {
		this.result = Math.sin(this.result);
	}
};

//НАследование вар.2
SciCalc.prototype = new Calc;
