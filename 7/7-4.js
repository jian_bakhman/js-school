/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// пространства имен

function clone(obj) {
	if (obj === null || typeof (obj) !== 'object' || 'isActiveClone' in obj)
		return obj;

	var temp = obj.constructor();

	for (var key in obj) {
		if (Object.prototype.hasOwnProperty.call(obj, key)) {
			obj['isActiveClone'] = null;
			temp[key] = clone(obj[key]);
			delete obj['isActiveClone'];
		}
	}

	return temp;
}