/*
 * ДЗ от 02.02.2016г.
 *
 * Жиан Бахман
 */


var calc = (function() {

//	функция convert(val) - конвертирует val в число, если val - число или строка
	function convert(val) {
		return +val;
	}

//	функция check(val_1, val_2) - которая будет содержать все проверки типа !isNaN(...)
	function check(val_1, val_2) {
		
		val_1 = convert(val_1);
		val_2 = convert(val_2);
		
		return !isNaN(val_1) && !isNaN(val_2);
	}
	
	function countExp(exp) {
		var operation = exp[0].match(/[\+\-\*\/]/);
		var val = exp[0].split(operation[0]);

		a = parseFloat(val[0]);
		b = parseFloat(val[1]);
		
		if (operation[0] == '+') return calc.add(a, b);
		if (operation[0] == '-') return calc.sub(a, b);
		if (operation[0] == '*') return calc.multipl(a, b);
		if (operation[0] == '+') return calc.divide(a, b);
		
	}

	return {
		add: function (a, b) {
			if (check(a, b)) {
				return convert(a) + convert(b);
			}
		},

		sub: function (a, b) {
			if (check(a, b)) {
				return a - b;
			}
		},

		multipl: function (a, b) {
			if (check(a, b)) {
				return a * b;
			}
		},

		divide: function(a, b) {
			if (check(a, b)) {
				if (b === 0) {
					return 'Деление на ноль';
				} else {
					return a / b;
				}
			}
		},
		
		calcExp: function (exp) {

			console.log(exp.match(/[\+\-\*\/]/));
			if (exp.match(/[\+\-\*\/]/)) {
				console.log('TRUE');
				var simpleExp = exp.match(/[\d]+[*/][\d]+/);
				console.log(simpleExp, '<br>');
				exp = exp.replace(simpleExp, countExp(simpleExp));
				console.log(exp, '<br>');
			
			}

		}
	};
}
)();


var val_1 = "102";
var val_2 = 5;

//var val_1 = 100;
//var val_2 = 5;

document.write(calc.add(val_1, val_2), '<br>');
document.write(calc.sub(val_1, val_2), '<br>');
document.write(calc.multipl(val_1, val_2), '<br>');
document.write(calc.divide(val_1, val_2), '<br>');

var exp = '2+65/5+11*2-14';
var simpleExp = exp.match(/[\+\-\*\/]/);
document.write(exp, '<br>');
document.write(simpleExp);
document.write(calc.calcExp(exp));

