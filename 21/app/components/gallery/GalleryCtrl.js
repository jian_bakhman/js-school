angular.module("app").controller("GalleryCtrl", function($scope) {

	var gallery = $scope.gallery = [
		{
			id: 0,
			image: 'gallery-item-1-big.jpg',
			thumbnail: 'gallery-item-1.jpg'
		},
		{
			id: 1,
			image: 'gallery-item-2-big.jpg',
			thumbnail: 'gallery-item-2.jpg'
		},
		{
			id: 2,
			image: 'gallery-item-3-big.jpg',
			thumbnail: 'gallery-item-3.jpg'
		},
		{
			id: 3,
			image: 'gallery-item-4-big.jpg',
			thumbnail: 'gallery-item-4.jpg'
		},
		{
			id: 4,
			image: 'gallery-item-5-big.jpg',
			thumbnail: 'gallery-item-5.jpg'
		},
		{
			id: 5,
			image: 'gallery-item-6-big.jpg',
			thumbnail: 'gallery-item-6.jpg'
		},
		{
			id: 6,
			image: 'gallery-item-7-big.jpg',
			thumbnail: 'gallery-item-7.jpg'
		},
		{
			id: 7,
			image: 'gallery-item-8-big.jpg',
			thumbnail: 'gallery-item-8.jpg'
		}
	];

	$scope.popup = false;

	$scope.popupScreen = function (e) {
		$scope.popup = true;
		$scope.slide = gallery[e.target.id];
		$scope.slideNum = $scope.slide.id + 1;
	};

	$scope.myPopupClick = function ($event) {
		if ($event == 'next') {
			if ($scope.slideNum < gallery.length) {
				$scope.slideNum++;
				$scope.slide = gallery[$scope.slideNum - 1];
			}
		}
		if ($event == 'previous') {
			if ($scope.slideNum > 1) {
				$scope.slideNum--;
				$scope.slide = gallery[$scope.slideNum - 1];
			}
		}
	};

	$scope.show = function (e) {
		$scope.popup = e;
	};
});