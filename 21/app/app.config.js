angular.module("app").config(function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/");
  //
  // Now set up the states
  $stateProvider
    .state('home', {
      url: "/",
      controller: 'HomeCtrl',
      templateUrl: "app/components/home/home.html"
    })
    .state('contact', {
      url: "/contact",
      controller: 'ContactCtrl',
      templateUrl: "app/components/contact/contact.html"
    })
    .state('gallery', {
      url: "/gallery",
      controller: 'GalleryCtrl',
      templateUrl: "app/components/gallery/gallery.html"
    })
    .state('about', {
      url: "/about",
      controller: 'AboutCtrl',
      templateUrl: "app/components/about/about.html"
    });
});