/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var model = {
    curData : 0,
    firstData : null,
    operation : null,
    newData : true,
    memory : 0
};

calcApp.controller('calcAppCtrl', function ($scope){
    $scope.model = model;
    console.log(model);
    console.log($scope.model);
    
    $scope.clickButton = function ($event){
        
//        console.log(Number(model.curData));
        console.log($event);
        console.log($event.target.innerText);
        console.log($event.target.className);
        console.log(model.newData);
//        console.log($event);

        if ($event.target.className == 'scinm'){
            if (model.newData) {
                model.curData = '';
            }
            model.curData = model.curData + $event.target.innerText;
//			if (model.newData) {
				model.curData = model.curData.replace(/^00/, '0');
//			}
			switchNewData(false);
			
        } else if ($event.target.className == 'sciop' && $event.target.innerText.match(/[\+–÷×]/)){
			if (model.firstData && model.operation) {
				equal();
			}
            model.operation = $event.target.innerText;
            model.firstData = model.curData;
//            model.curData = model.curData + '.';
			switchNewData(true);

		} else if ($event.target.className == 'scieq'){
            if ($event.target.innerText == '='){
                equal();
				
            } else  if ($event.target.innerText == 'C'){
                model.curData = 0;
				model.firstData = null;
				model.operation = null;
				model.newData = false;
            }
        }

        function equal(){
            if (model.operation == '+'){
                model.curData = parseFloat(model.firstData) + parseFloat(model.curData);
            }
            if (model.operation == '–'){
                model.curData = parseFloat(model.firstData) - parseFloat(model.curData);
            }
            if (model.operation == '×'){
                model.curData = parseFloat(model.firstData) * parseFloat(model.curData);
            }
            if (model.operation == '÷'){
                model.curData = parseFloat(model.firstData) / parseFloat(model.curData);
            }
			
            model.firstData = null;
			model.operation = null;
			switchNewData(true);
        }
		
		function switchNewData(t) {
			model.newData = t;
		}
      
		console.log(model);
    };
});
