/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// СПИСОК   List


var first_elem = {
 data: 10,
 next: {
  data: 0,
  next: null
 }
};

// Tree    Дерево

var tree = {
 data: 0,
 childs: [
  {
   data: 10,
   childs: []
  },
  {
   data: 15,
   childs: []
  },
  {
   data: 100,
   childs: [
    {
     data: 25,
     childs: []
    },
    {
     data: 150,
     childs: []
    }
   ]
  },
  {
   data: 1500,
   childs: []
  }
 ]
};

function search(tr, data) {
 if (tr.data != data) {
  var result;

  for (var index = 0; index < tr.childs.length; index++) {
   result = result || search(tr.childs[index], data);
  }

  return result;
 } else {
  return tr;
 }
}


// Бинарное дерево

