/* 
 * Home work #10-1 from 2016-02-26
 *
 * построить меню, которое будет выпадать по клику
 * и отображать детей, если они есть
 */


var menu_json = [
	{
		"title": "Home",
		"menu": "home",
		"content": "home content"
	},
	{
		"title": "Products",
		"menu": "products",
		"content": "products content",
		"childs": [
			{
				"title": "Product 1",
				"menu": "product 1",
				"content": "111"
			},
			{
				"title": "Product 2",
				"menu": "product 2",
				"content": "222"
			},
			{
				"title": "Product 3",
				"menu": "product 3",
				"content": "333"
			}
		]
	},
	{
		"title": "Contact Us",
		"menu": "contact us",
		"content": "contact us content"
	}
];
//-----------------------------------------

//------------------------------------------------------------------

document.addEventListener("DOMContentLoaded", function() {
 var menuItems = null;
 var content = null;

 AJAX("content-template.html", "GET", function(contentData) {
  content = contentData;

  AJAX("menu-data.json", "GET", function(data) {
   menuItems = JSON.parse(data);

   var menuHolder = document.getElementById("menu");
   var contentHolder = document.getElementById("content");
   var ulElement = document.createElement("ul");

   for (var i = 0; i < menuItems.length; i++) {
    var liElement = document.createElement("li");
    liElement.innerText = menuItems[i].menu;

    liElement.onclick = (function(menuItem) {
     return function () {
      document.title = menuItem.title;
      contentHolder.innerHTML = contentData.replace("{{content}}", menuItem.content);
     }
    })(menuItems[i]);

    ulElement.appendChild(liElement);
   }

   menuHolder.appendChild(ulElement);
  });
 });
});