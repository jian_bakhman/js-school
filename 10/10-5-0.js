/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//hw 1:
[
  {
 "title": "Home",
 "menu": "home",
 "content": "home content"
  },
  {
 "title": "Products",
 "menu": "products",
 "content": "products content",
 "childs": [
   {
  "title": "Product 1",
  "menu": "product 1",
  "content": "111"
   },
   {
  "title": "Product 2",
  "menu": "product 2",
  "content": "222"
   },
   {
  "title": "Product 3",
  "menu": "product 3",
  "content": "333"
   }
 ]
  },
  {
 "title": "Contact Us",
 "menu": "contact us",
 "content": "contact us content"
  }
]
/*
построить меню, которое будет выпадать по клику
и отображать детей, если они есть*/
//-----------------------------------------
/*
hw 2:
переписать функцию AJAX таким образом
чтобы можно было обрабатывать паралельно несколько запросов
кол-во запросов задается параметром
*/
var AJAX = function(url, method, callback) {
 var xhttp = new XMLHttpRequest();

 xhttp.onreadystatechange = function() {
  if (xhttp.readyState == 4 && xhttp.status == 200) {
   callback.call(null, xhttp.responseText);
  }
 };
 xhttp.open(method, url, true);
 xhttp.send();
};
//функция будет вида function(array, num_of_parallel_calls) {

//------------------------------------------------------------------

document.addEventListener("DOMContentLoaded", function() {
 var menuItems = null;
 var content = null;

 AJAX("content-template.html", "GET", function(contentData) {
  content = contentData;

  AJAX("menu-data.json", "GET", function(data) {
   menuItems = JSON.parse(data);

   var menuHolder = document.getElementById("menu");
   var contentHolder = document.getElementById("content");
   var ulElement = document.createElement("ul");

   for (var i = 0; i < menuItems.length; i++) {
    var liElement = document.createElement("li");
    liElement.innerText = menuItems[i].menu;

    liElement.onclick = (function(menuItem) {
     return function () {
      document.title = menuItem.title;
      contentHolder.innerHTML = contentData.replace("{{content}}", menuItem.content);
     }
    })(menuItems[i]);

    ulElement.appendChild(liElement);
   }

   menuHolder.appendChild(ulElement);
  });
 });
});