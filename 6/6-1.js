/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function listComplex(arr) {
	if (arr.length === 0) {
		return null;
	}

	return {
		data: arr[0],
		next: listComplex(arr.shift())
	};
}

function listLength(list) {
 if (list.next == null) {
  return 1;
 } else {
  return 1 + listLength(list.next);
 }
}


 
var obj = {
	a: 'test',
	b: null,
	d: 100500,
	x: function (val) {
		alert();
	},
	xx: {
		a: "test 2",
		d: 100
	}
};

function objList(obj) {
	for (var key in obj) {
		if (obj.hasOwnProperty(key)) {
			if (typeof (obj[key]) === 'object' && obj[key] !== null) {
				objList(obj[key]);
			} else {
				console.log(obj[key]);			
			}
		}
	}
}


var obj = {
	a: "test",
	b: null,
	d: 100500,
	x: function (val) {
		alert(val);
	},
	xx: {
		a: "test 2",
		d: 100,
		xxx: {
			f: function () {
			},
			g: true
		}
	},
	xxxx: {
		n: "test 3",
		g: false
	}
};

function deepCopy(parameters) {
	var newObj = {};
	for (var key in parameters) {
		if (parameters.hasOwnProperty(key)) {
			if (typeof (parameters[key]) === 'object' && parameters[key] !== null) {
				newObj[key] = deepCopy(parameters[key]);
			} else {
				newObj[key] = parameters[key];			
			}
		}
	}
	return  newObj;
}

var copy = deepCopy(obj);
obj.xx.xxx.g = false;
console.log(copy.xx.xxx.g);


var obj = {
	a: "test",
	b: null,
	d: 100500,
	x: function (val) {
		alert(val);
	},
	xx: {
		a: "test 2",
		d: 100,
		xxx: {
			f: function () {
			},
			g: true
		}
	},
	xxxx: {
		n: "test 3",
		g: false
	},
	y: [
		100,
		200,
		"test 77"
	]
};

function deepCopy(parameters) {
	var newObj = {};
	for (var key in parameters) {
		if (parameters.hasOwnProperty(key)) {
			if (typeof (parameters[key]) === 'object' && parameters[key] !== null) {
				newObj[key] = deepCopy(parameters[key]);
			} else if (Object.prototype.toString.call( parameters[key] ) === '[object Array]') {
				newObj[key].push(parameters[key]);
			} else {
				newObj[key] = parameters[key];			
			}
		}
	}
	return  newObj;
}

//Object.prototype.toString.call( someVar ) === '[object Array]'

var copy = deepCopy(obj);
obj.xx.xxx.g = false;
console.log(copy.xx.xxx.g);

copy.y = (350);
console.log(obj.y.length);