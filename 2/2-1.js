/* 
 * ДЗ от 29.01.16
 * 
 * Бахман Жиан
 *
 */

/*
 * Калькулятор простой 
 */

function add(a, b) {
	return a + b;
}

function sub(a, b) {
	return a - b;
}

function multipl(a, b) {
	return a * b;
}

function div(a, b) {
	if (b === 0) {
		return('Деление на ноль');
	} else {
		return a / b;
	}
}

var a = '5+';
var b = '+10';

a = parseFloat(a);
b = parseFloat(b);

if (typeof(a) !== 'number' && typeof(b) !== 'number') {
	document.write('Одно или оба значения не численные!');
} else {
	document.write('-сложение: ', add(a, b), '<br>');
	document.write('-вычитание: ', sub(a, b), '<br>');
	document.write('-умножение: ', multipl(a, b), '<br>');
	document.write('-деление: ', div(a, b), '<br>');
}


/*
 * Калькулятор с определением арифметического действия
 */

var mathExpr = '5 - 10';

var regexp = /[\+\-\*\/]/;
var operation = mathExpr.match(regexp);
var val = mathExpr.split(operation[0]);

a = parseFloat(val[0]);
b = parseFloat(val[1]);

switch (operation[0]) {
	case '+':
		document.write('-сложение: ', add(a, b), '<br>');
		break;
	case "-":
		document.write('-вычитание: ', sub(a, b), '<br>');
		break;
	case '*':
		document.write('-умножение: ', multipl(a, b), '<br>');
		break;
	case '/':
		document.write('-деление: ', div(a, b), '<br>');
		break;
	default:
		document.write('Я таких значений не знаю');
}


