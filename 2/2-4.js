/* 
 * 2016-02-05
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function closure() {
	var test = 10;
}


// Массивы
var arr = [];  // new Array()
arr.push('test');	// add to array
arr.push(10);	// add to array


// typed array

var tArray = (function () {
    var arr = [];

    return {
        push: function (val) {
            if (typeof val == "number") {
                arr.push(val);
            }
        },
        show: function () {
            return JSON.parse(JSON.stringify(arr));
        }
    }
})();

tArray.push(10);
tArray.push(7);
tArray.push("test");

console.log(tArray.show());


//  циклы
var sum = 0;
for (var i = 0; i < 10; i++){
	sum++;
}

// аналог написания for 
var i = 0;
for (; i , 10;) {
	sum++;
	
	i++;
}

var sum1 = 0;



/*
 * Типы циклов
 * 
 * for (i = 0; i  < 10; i++) {    }
 * 
 *  while (i < 10) 
 */

var j = 0;
//do


var arr = [10, 3, 2];