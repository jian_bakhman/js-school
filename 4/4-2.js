/* 
 * ДЗ 05.02.2016
 * 
 * Жиан Бахман
 */

/* Задание №1.
 * Цикл сортировки массива 
 * var arr = [3, 7, -2, -5, 6, 0 ,10]; 
 * Нужно получить результат  [-5, -2, 0, 3, 6, 7, 10]
 */
document.write('Задание №1.<br>');

var arr = [3, 7, -2, -5, 6, 0 ,10];
document.write(arr, '<br>');

function mySort(arr) {
    var middle;
    for (var i = arr.length; i > 1; i--) {
	for (var j =0; j < i-1; j++) {
	    if (arr[j] > arr[j+1]) {
		middle = arr[j];
		arr[j] = arr[j+1];
		arr[j+1] = middle;
	    }
	}
    }
    return arr;
};
    
document.write(mySort(arr));


document.write('<br><br><br>');

/*
 * Задание №2.
 * Есть строка "10+3-7+20-13.2+3" (нет * и /)
 * Нужно посчитать результат используя цикл и indexOf.
 * Для дробных чисел - разделитель точка.
 */
document.write('Задание №2.<br>');

var expr = "10+3-7+20-13.2+3";
document.write(expr, ' = ');
var pos = 0;
var oldPos = 0;

var posArr = [];
pos = expr.indexOf('-');
while (pos !== -1) {
    posArr.push(pos);
    pos = expr.indexOf('-', pos + 1);
}
pos = expr.indexOf('+');
while (pos !== -1) {
    posArr.push(pos);
    pos = expr.indexOf('+', pos + 1);
}
posArr.sort(function (a, b){
    return a-b;
});

var sum = 0;
pos = 0;
var num;
for (var k = 0; k <= posArr.length; k++) {
    num = expr.substring(pos, posArr[k]);
    if (expr.substr(posArr[k-1], 1) !== '-'){
        sum = sum + parseFloat(num);
    } else {
        sum = sum - parseFloat(num);
    }
    pos = posArr[k]+1;
}
document.write(sum);
